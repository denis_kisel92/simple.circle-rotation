/**
 * Created by ����� on 23.11.2016.
 */
function Detector(circle, rotation, angular, distance)
{
    this.circle = circle;

    this.rotation = document.getElementById(rotation);
    this.angular = document.getElementById(angular);
    this.distance = document.getElementById(distance);

    this.currentDistance = 0;

    this.timeoutID = '';

    this.constructor = function() {
        clearTimeout(this.timeoutID);

        this.setRotation();
        this.setAngular();
        this.setDistance();

        var selfObj = this;
        this.timeoutID = setTimeout(function(){selfObj.constructor()}, 10);
    };

    this.setRotation = function() {
        var minuteToSecond = 60;
        this.rotation.value = (this.circle.turnoversOnSecond * minuteToSecond).toFixed(2);
    };

    this.setAngular = function() {
        this.angular.value = this.circle.angularVelocity.toFixed(2);
    };

    this.setDistance = function() {
        var smToMeter = 100;
        var circleRadiusToMeter = this.circle.radius/smToMeter;

        var distance = this.circle.angularVelocity * this.circle.absoluteTimeRendering * 2 * circleRadiusToMeter;
        this.currentDistance += distance; // Distance = w*t*2r
        this.currentDistance = Number(this.currentDistance.toFixed(2));

        this.distance.value = this.currentDistance;
    };
}