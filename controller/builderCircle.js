function BuilderCircle(circle, controlCircleArg, detectorArg)
{
    this.constructor = function() {

        new Detector(
            circle,
            detectorArg.rotation,
            detectorArg.angular,
            detectorArg.distance
        ).constructor();

        var controlCircle = new ControlCircle(
            circle,
            controlCircleArg.speed,
            controlCircleArg.radius,
            controlCircleArg.start
        ).constructor();

        return controlCircle;
    };
}