/**
 * Created by ����� on 11/18/2016.
 */
function Circle(circleId, speed, radius) {
    this.PI = 3.1415;

    this.circle = document.getElementById(circleId);

    this.speedBeforeStop = 0;
    this.speed = (speed) ? speed : 30;
    this.radius = (radius) ? radius : 30;
    this.angularVelocity = 0;
    this.turnoversOnSecond = 0;

    this.currentRotation = 0;

    this.timeRendering = 10;
    this.millisecondOfSecond = 1000;
    this.absoluteTimeRendering = this.timeRendering/this.millisecondOfSecond;

    this.startRotation = function() {

        clearTimeout(this.timeoutRotationId);

        var radius = this.radius;
        var speed = this.speed;

        var circleLength = this.getCircleLenght(radius);
        var speedMeterToSecond = this.convertSpeedKmToM(speed);

        var turnoversOnSecond = this.getTurnoversOnSecond(speedMeterToSecond, circleLength);
        var angularVelocity = this.getAngularVelocity(turnoversOnSecond);
        var angularVelocityOnDegree = this.convertRadianToDegree(angularVelocity);

        this.updateParams(turnoversOnSecond, angularVelocity);

        this.currentRotation += angularVelocityOnDegree * this.absoluteTimeRendering;
        this.setRotateCircle(this.currentRotation);

        var selfObj = this;
        this.timeoutRotationId = setTimeout(function() {selfObj.startRotation();}, this.timeRendering);
    };

    this.stopRotation = function() {
        clearTimeout(this.timeoutRotationId);
        this.speedBeforeStop = this.speed;
        this.speed = 0;
        this.angularVelocity = 0;
        this.turnoversOnSecond = 0;
    };

    this.updateParams = function(turnoversOnSecond, angularVelocity) {
        this.turnoversOnSecond = Number(turnoversOnSecond.toFixed(4));
        this.angularVelocity = Number(angularVelocity.toFixed(4));
    };

    this.getCircleLenght = function(radius) {
        var length = 2 * radius * this.PI; // L = 2PiR

        var smInMeter = 100;
        var lengthToMeter = length/smInMeter;
        return Number(lengthToMeter.toFixed(4));
    };

    //convert km/h to m/s
    this.convertSpeedKmToM = function(speed) {
        var result = speed * 10/36; //speed * 1000meter / 3600second
        return Number(result.toFixed(4));
    };

    this.getTurnoversOnSecond = function(speedMeterOnSecond, circleLength) {
        var result = speedMeterOnSecond/circleLength;
        return Number((result).toFixed(4));
    };

    this.getAngularVelocity = function(turnoversOnSecond) {
        var result = 2 * this.PI * turnoversOnSecond; //w = 2PiF (rad/s)
        return Number(result.toFixed(4));
    };

    this.convertRadianToDegree = function(radian) {
        var relativeRadian = 57.29; //1rad = 57.29degree;
        var result = radian * relativeRadian; // 1degree = 2Pi/360;
        return Number(result.toFixed(4));
    };

    this.setRotateCircle = function(degree) {
        this.circle.style.transform = 'rotate(' + degree + 'deg)';
    };

}