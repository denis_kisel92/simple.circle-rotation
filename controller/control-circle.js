/**
 * Created by ����� on 11/18/2016.
 */

function ControlCircle(circle, speedId, radiusId, startId)
{

    this.circle = circle;

    this.speed = document.getElementById(speedId);
    this.radius = document.getElementById(radiusId);
    this.start = document.getElementById(startId);


    this.constructor = function() {
        this.init();

        this.changeRadius();
        this.changeSpeed();
        this.startStop();

        return this;
    };

    this.init = function() {
        this.speed.value = 15;
        this.radius.value = 15;

        this.circle.speed = this.speed.value;
        this.circle.radius = this.speed.value;
    };

    this.changeSpeed = function() {
        var selfObj = this;

        this.speed.onchange = function() {
            selfObj.circle.speed = this.value;
        };
    };

    this.changeRadius = function() {
        var selfObj = this;

        this.radius.onchange = function() {
            selfObj.circle.radius = this.value;
        };
    };

    this.startStop = function() {
        var selfObj = this;

        this.start.onclick = function() {
            if (selfObj.circle.speed == 0) {
                selfObj.circle.speed = selfObj.circle.speedBeforeStop;
                selfObj.startCircle();
            } else {
                selfObj.stopCircle();
            }
        };
    };

    this.startCircle = function() {
        return this.circle.startRotation();
    };

    this.stopCircle = function() {
        return this.circle.stopRotation();
    };

}