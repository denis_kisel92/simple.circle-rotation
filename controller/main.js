/**
 * Created by ����� on 11/18/2016.
 */
var controlCircle = new BuilderCircle(
    new Circle('circle'),
    {
        speed: 'speed',
        radius: 'radius',
        start: 'start'
    },
    {
        rotation: 'rotation',
        angular: 'angular',
        distance: 'distance'
    }
).constructor();

controlCircle.startCircle();