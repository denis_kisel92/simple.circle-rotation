/**
 * Created by ����� on 19.11.2016.
 */

// default settings
var speed = 72; //km/h
var radius_1 = 15; //sm
var radius_2 = 30; //sm

var circle = new Circle('circle');
circle.speed = speed;

QUnit.test("circle.getCircleLenght", function( assert ) {
    assert.equal(circle.getCircleLenght(radius_1), 0.9425);
    assert.equal(circle.getCircleLenght(radius_2), 1.8849);
});

QUnit.test("circle.convertSpeedKmToM", function( assert ) {
    assert.equal(circle.convertSpeedKmToM(speed), 20);
});

QUnit.test("circle.getTurnoversOnSecond", function( assert ) {
    var speedMeterOnSecond = 20;
    var circleLength_1 = 94.245;
    var circleLength_2 = 188.49;
    assert.equal(circle.getTurnoversOnSecond(speedMeterOnSecond, circleLength_1), 0.2122);
    assert.equal(circle.getTurnoversOnSecond(speedMeterOnSecond, circleLength_2), 0.1061);
});

QUnit.test("circle.getAngularVelocity", function( assert ) {
    var turnoversOnSecond_1 = 0.2122;
    var turnoversOnSecond_2 = 0.1061;
    assert.equal(circle.getAngularVelocity(turnoversOnSecond_1), 1.3333);
    assert.equal(circle.getAngularVelocity(turnoversOnSecond_2), 0.6666);
});

QUnit.test("circle.convertRadianToDegree", function( assert ) {
    var radian_1 = 1.3333;
    var radian_2 = 0.6666;
    assert.equal(circle.convertRadianToDegree(radian_1), 76.3848);
    assert.equal(circle.convertRadianToDegree(radian_2), 38.1895);
});